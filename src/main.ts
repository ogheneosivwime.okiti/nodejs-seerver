function start(): void {
  try {
    console.log('Hello world!'); // eslint-disable-line no-console
  } catch (error: unknown) {
    console.log(error); // eslint-disable-line no-console
    process.exit(1);
  }
}

start();
